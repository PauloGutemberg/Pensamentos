//
//  SettingsViewController.swift
//  Pensamentos
//
//  Created by Paulo Gutemberg on 02/01/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
	
	@IBOutlet weak var swAuthorFresh: UISwitch!
	@IBOutlet weak var slTimeInterval: UISlider!
	@IBOutlet weak var scColorScheme: UISegmentedControl!
	@IBOutlet weak var lbTimeInterval: UILabel!
	
	let config = Configuration.compartilhar
	
    override func viewDidLoad() {
        super.viewDidLoad()
		//Adicinar um observador para quando a aplicacao se tonar ativar
		NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Atualizar"), object: nil, queue: nil) { (notificacao) in
			
			self.formataView()
		}
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		formataView()
	
	}
	
	func formataView(){
		swAuthorFresh.setOn(config.atualizacaoAutomatica, animated: false)
		slTimeInterval.setValue(Float(config.intervaloDeTempo), animated: false)
		scColorScheme.selectedSegmentIndex = config.esquemaDeCores
		
		mudarLabelDoItervaloDeTempo(com: config.intervaloDeTempo)
	}
    
	func mudarLabelDoItervaloDeTempo(com intervalo: Double){
		lbTimeInterval.text = "Mudar após \(Int(intervalo)) segundos"
	}
	
	@IBAction func changeAutoFresh(_ sender: UISwitch){
		config.atualizacaoAutomatica = sender.isOn
	}
	
	@IBAction func changeTimeInterval(_ sender: UISlider){
		let valor = Double(round(sender.value))
		mudarLabelDoItervaloDeTempo(com: valor)
		config.intervaloDeTempo = valor
	}
	
	@IBAction func changeColorScheme(_ sender: UISegmentedControl){
		config.esquemaDeCores = sender.selectedSegmentIndex
	}

}
