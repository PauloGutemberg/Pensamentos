//
//  QuotesViewController.swift
//  Pensamentos
//
//  Created by Paulo Gutemberg on 02/01/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import UIKit

class QuotesViewController: UIViewController {
	
	@IBOutlet weak var ivPhoto: UIImageView!
	@IBOutlet weak var ivPhotoBg: UIImageView!
	@IBOutlet weak var lbQuote: UILabel!
	@IBOutlet weak var lbAuthor: UILabel!
	@IBOutlet weak var lcTop: NSLayoutConstraint!
	//LC de layout constrants
	
	let gerenciadorDePensamentos = QuoteManager()
	var timer: Timer? //sempre que queremos agendar algo em determinado tempo
	
	let config = Configuration.compartilhar
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.ivPhotoBg.alpha = 0.25
		//Adicinar um observador para quando a aplicacao se tonar ativar
		NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Atualizar"), object: nil, queue: nil) { (notificacao) in
			
			self.formataView()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		formataView()
		prepararPensamento()
	}
	
	func formataView(){
		view.backgroundColor = config.esquemaDeCores == 0 ? .white : UIColor(red: 156.0/255.0, green: 68.0/255.0, blue: 15.0/255.0, alpha: 1.0)
		
		lbQuote.textColor = config.esquemaDeCores == 0 ? .black : .white
		lbAuthor.textColor = config.esquemaDeCores == 0 ?  UIColor(red: 192.0/255.0, green: 96.0/255.0, blue: 49.0/255.0, alpha: 1.0) : .yellow
		
	}
	
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		prepararPensamento()
	}
	
	func prepararPensamento(){
		
		timer?.invalidate() //para sempre o agendamento caso esteja acontecendo
		if config.atualizacaoAutomatica == true {
			//implementa o agendamento de algo e constroi um timer
			timer = Timer.scheduledTimer(withTimeInterval: config.intervaloDeTempo, repeats: true) { (timer) in
				self.mostrarPensamentoAleatorio()
			}
		}
		mostrarPensamentoAleatorio()
	}
	
	func mostrarPensamentoAleatorio(){
		let pensa = gerenciadorDePensamentos.getPensamentoRandom()
		self.lbQuote.text = pensa.pensamento
		self.lbAuthor.text = pensa.autor
		
		self.ivPhoto.image = UIImage(named: pensa.imagem)
		self.ivPhotoBg.image = self.ivPhotoBg.image
		
		self.lbQuote.alpha = 0.0
		self.lbAuthor.alpha = 0.0
		self.ivPhoto.alpha = 0.0
		self.ivPhotoBg.alpha = 0.0
		
		self.lcTop.constant = 50
		self.view.layoutIfNeeded() //sempre que quer redesenhar na tela
		
		UIView.animate(withDuration: 2.5) {
			self.lbQuote.alpha = 1.0
			self.lbAuthor.alpha = 1.0
			self.ivPhoto.alpha = 1.0
			self.ivPhotoBg.alpha = 0.25
			
			self.lcTop.constant = 10
			self.view.layoutIfNeeded()
		}
	}
	
}
