//
//  Quote.swift
//  Pensamentos
//
//  Created by Paulo Gutemberg on 02/01/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

struct Quote: Codable {
	let pensamento: String
	let autor: String
	let imagem: String
	
	
	var pensamentoFormatado: String {
		return "‟"+self.pensamento+"“"
	}
	///add Emoji cmd + control + space
	
	var autorFormatadp: String {
		return "- "+self.autor+" -"
	}
	
	
    // keys de acordo com o protocol Encodable e Decodable
    private enum CodingKeys: String, CodingKey {
        case pensamento = "quote"
        case autor = "author"
        case imagem = "image"
    }
}

/*

Codable é um apelido (typeAlias) para mais dois protocolos
Encodable e Decodable

*/
