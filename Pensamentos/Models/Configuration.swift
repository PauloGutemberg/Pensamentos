//
//  Configuration.swift
//  Pensamentos
//
//  Created by Paulo Gutemberg on 02/01/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

enum UsuarioPadraoChaves: String {
	case intervaloDeTempo = "timeInterval"
	case esquemaDeCores = "colorScheme"
	case atualizacaoAutomatica = "autoRefresh"
}

class Configuration {
	
	let padrao = UserDefaults.standard
	static var compartilhar: Configuration = Configuration()
	
	var intervaloDeTempo: Double {
		get{
			return padrao.double(forKey: UsuarioPadraoChaves.intervaloDeTempo.rawValue)
		}
		
		set{
			padrao.set(newValue, forKey: UsuarioPadraoChaves.intervaloDeTempo.rawValue)
		}
	}
	
	var esquemaDeCores: Int {
		get{
			return padrao.integer(forKey: UsuarioPadraoChaves.esquemaDeCores.rawValue)
		}
		set{
			padrao.set(newValue, forKey: UsuarioPadraoChaves.esquemaDeCores.rawValue)
		}
	}
	
	var atualizacaoAutomatica: Bool{
		get{
			return padrao.bool(forKey: UsuarioPadraoChaves.atualizacaoAutomatica.rawValue)
		}		
		set{
			padrao.set(newValue, forKey: UsuarioPadraoChaves.atualizacaoAutomatica.rawValue)
		}
	}
	
	private init(){
		if padrao.double(forKey: UsuarioPadraoChaves.intervaloDeTempo.rawValue) == 0 {
			padrao.set(8.0, forKey: UsuarioPadraoChaves.intervaloDeTempo.rawValue)
		}
	}
}
