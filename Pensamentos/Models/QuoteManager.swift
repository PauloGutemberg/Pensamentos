//
//  QuoteManager.swift
//  Pensamentos
//
//  Created by Paulo Gutemberg on 02/01/20.
//  Copyright © 2020 Paulo Gutemberg. All rights reserved.
//

import Foundation

class QuoteManager {
	
	let pensamentos: [Quote]
	
	init() {
		let fileUrl = Bundle.main.url(forResource: "quotes", withExtension: "json")!
		let jsonData = try! Data(contentsOf: fileUrl)
		let jsonDecoder = JSONDecoder()
		pensamentos = try! jsonDecoder.decode([Quote].self, from: jsonData)
	}
	
	func getPensamentoRandom() -> Quote{
		//pega o index de um pensamento aleatorio
		let index = Int(arc4random_uniform(UInt32(self.pensamentos.count)))
		return pensamentos[index]
	}
}
